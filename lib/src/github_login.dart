import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:oauth2/oauth2.dart' as oauth2;
import 'package:url_launcher/url_launcher.dart';

var authUrl = 'https://github.com/login/oauth/authorize';
var accessTokenUrl = 'https://github.com/login/oauth/access_token';
final _authorizationEndpoint = Uri.parse(authUrl);
final _tokenEndpoint = Uri.parse(accessTokenUrl);

class GithubLogin extends StatefulWidget {
  const GithubLogin(
      {required this.builder,
      required this.githubClientId,
      required this.githubClientSecret,
      required this.githubScopes,
      Key? key})
      : super(key: key);
  final AuthenticatedBuilder builder;
  final String githubClientId;
  final String githubClientSecret;
  final List<String> githubScopes;

  @override
  State<GithubLogin> createState() => _GithubLoginState();
}

typedef AuthenticatedBuilder = Widget Function(
    BuildContext context, oauth2.Client client);

class _GithubLoginState extends State<GithubLogin> {
  HttpServer? _redirectServer;
  oauth2.Client? _client;

  @override
  Widget build(BuildContext context) {
    final client = _client;
    if (client != null) {
      return widget.builder(context, client);
    }
    return Scaffold(
        appBar: AppBar(
            title: const Text(
          'Login',
          style: TextStyle(fontFamily: 'Gotham'),
        )),
        body: Center(
          child: ElevatedButton(
            onPressed: () async {
              await _redirectServer?.close();
              _redirectServer = await HttpServer.bind('localhost', 0);
              var authenticatedHttpClient = await _getOAuth2Client(
                  Uri.parse('http://localhost:${_redirectServer!.port}/auth'));
              setState(() {
                _client = authenticatedHttpClient;
              });
            },
            child: const Text(
              'Login to GitHub',
              style: TextStyle(
                fontFamily: 'Gotham',
              ),
            ),
          ),
        ));
  }

  Future<oauth2.Client> _getOAuth2Client(Uri redirectUrl) async {
    if (widget.githubClientId.isEmpty || widget.githubClientSecret.isEmpty) {
      throw const GithubLoginException(
          'githubClientId and githubClientSecret must be not empty. '
          'See `lib/github_oauth_credentials.dart` for more detail.');
    }
    var grant = oauth2.AuthorizationCodeGrant(
      widget.githubClientId,
      _authorizationEndpoint,
      _tokenEndpoint,
      secret: widget.githubClientSecret,
      httpClient: _JsonAcceptingHttpClient(),
    );
    var authUrl =
        grant.getAuthorizationUrl(redirectUrl, scopes: widget.githubScopes);
    await _chuyenhuong(authUrl);
    var resQuery = await _nghe();
    var client = await grant.handleAuthorizationResponse(resQuery);
    return client;
  }

  Future<void> _chuyenhuong(Uri authUrl) async {
    var url = authUrl.toString();
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw GithubLoginException("Can't lauch $url");
    }
  }
  Future<Map<String,String>>_nghe()async{
    var request = await _redirectServer!.first;
    var params = request.uri.queryParameters;
    request.response.statusCode = 200;
    request.response.headers.set('content-type', 'text/plain');
    request.response.writeln('Authenticated! You can close this tab.');
    await request.response.close();
    await _redirectServer!.close();
    _redirectServer = null;
    return params;

  }
}
class _JsonAcceptingHttpClient extends http.BaseClient {
  final _httpClient = http.Client();
  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers['Accept'] = 'application/json';
    return _httpClient.send(request);
  }
}
class GithubLoginException implements Exception {
  const GithubLoginException(this.message);
  final String message;
  @override
  String toString() => message;
}